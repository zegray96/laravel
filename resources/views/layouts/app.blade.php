<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel @yield('tituloPagina')</title>

    @include('layouts/links')

</head>
<body>
    @include('layouts/header')

    {{-- ACA IRIA MI CONTENIDO --}}
    @yield('contenido')
    
    @include('layouts/footer')

    @include('layouts/scripts')
</body>
</html>