@extends('layouts/app')

@section('tituloPagina', ' | Nuevo Cliente')


@section('contenido')
    <div>
        <h1>Nuevo Cliente</h1>

        <button>Atras</button>

        <form action="{{ route('clientes.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Nombre" value="{{ old('name') }}" name="name">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Apellido" value="{{ old('surname') }}"
                        name="surname">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Edad" value="{{ old('age') }}"
                        name="age">
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <button type="submit" class="btn btn-success">Enviar</button>
        </form>


    </div>
@endsection

@section('script-especifico')
    {{-- <script>
        alert('Entrado a pagina de clientes')
    </script> --}}
@endsection
