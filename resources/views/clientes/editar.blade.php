@extends('layouts/app')

@section('tituloPagina', ' | Nuevo Cliente')


@section('contenido')
    <div>
        <h1>Editar Cliente Id: {{$client->id}}</h1>
        
        <form action="{{ route('clientes.update', $client->id) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Nombre" value="{{ old('name') ? old('name') : $client->name }}" name="name">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Apellido" value="{{ old('surname') ? old('surname') : $client->surname }}"
                        name="surname">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Edad" value="{{ old('age') ? old('age') : $client->age }}"
                        name="age">
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <button type="submit" class="btn btn-success">Enviar</button>
        </form>

    </div>
@endsection

@section('script-especifico')
    {{-- <script>
        alert('Entrado a pagina de clientes')
    </script> --}}
@endsection
