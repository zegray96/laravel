@extends('layouts/app')

@section('tituloPagina', ' | Clientes')


@section('contenido')
    <div>
        <h1>Clientes</h1>

        @if (Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{ Session::get('success') }}</li>
                </ul>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ Session::get('error') }}</li>
                </ul>
            </div>
        @endif

        <a href="{{ route('clientes.nuevo') }}" class="btn btn-success">Nuevo</a>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th>Accion</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client)
                    <tr>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->surname }}</td>
                        <td>
                            <a href="{{ route('clientes.editar', $client->id) }}" class="btn btn-success">Editar</a>

                            <form action="{{ route('clientes.eliminar', $client->id) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>

                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
@endsection

@section('script-especifico')
    {{-- <script>
        alert('Entrado a pagina de clientes')
    </script> --}}
@endsection
