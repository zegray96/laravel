<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::get();

        return view('clientes.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
        ]);

        try {
            DB::beginTransaction();
            
            Client::create([
                'names' => $request->name,
                'surname' => $request->surname,
                'age' => 20,
                'country_id' => 1
            ]);
            
            DB::commit();


            return redirect()->route('clientes.index')->with('success', 'El registro se creo con exito!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('clientes.index')->with('error', 'No se pudo crear porque: '.$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clientes.editar', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
        ]);

        try {
            DB::beginTransaction();
            

            $client->name = $request->name;
            $client->surname = $request->surname;
            $client->age = 20;
            $client->country_id = 1;
            $client->save();
            
            DB::commit();


            return redirect()->route('clientes.index')->with('success', 'El registro se edito con exito!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('clientes.index')->with('error', 'No se pudo editar porque: '.$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        try {
            DB::beginTransaction();
            
            $client->delete();
            
            DB::commit();


            return redirect()->route('clientes.index')->with('success', 'El registro se elimino!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('clientes.index')->with('error', 'No se pudo eliminar porque: '.$e->getMessage());
        }
    }
}