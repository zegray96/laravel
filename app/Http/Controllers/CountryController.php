<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Country;

class CountryController extends Controller
{
    public function getAll()
    {
        // $countries = DB::table('countries')
        // ->select('name')
        // ->where('id', '=', '2')
        // ->get();

        $countries = Country::getCountryById(1);

        $country = Country::get();
        
        return $country;
    }
}