<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    public function scopeGetCountryById($q, $id)
    {
        return $q->select('name')
        ->where('id', '=', $id)
        ->orWhere('id', '=', '2')
        ->first();
    }
}