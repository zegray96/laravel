<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/otraruta', function () {
    return "Viendo otra ruta";
});

// CLIENTES
Route::prefix('/clientes')->group(function () {
    Route::get('/', [Controllers\ClientController::class, 'index'])->name('clientes.index');
    Route::get('/nuevo', [Controllers\ClientController::class, 'create'])->name('clientes.nuevo');
    Route::post('/store', [Controllers\ClientController::class, 'store'])->name('clientes.store');
    Route::put('/update/{client}', [Controllers\ClientController::class, 'update'])->name('clientes.update');
    Route::get('/editar/{client}', [Controllers\ClientController::class, 'edit'])->name('clientes.editar');
    Route::delete('/eliminar/{client}', [Controllers\ClientController::class, 'destroy'])->name('clientes.eliminar');
});



Route::get('/articulos', function () {
    return view('articulos');
});

Route::get('/getCountries', [Controllers\CountryController::class, 'getAll']);

Route::get('/people', [Controllers\PersonController::class, 'index']);